resource "google_compute_disk" "disk" {
  count = "${var.num_disks * length(var.zones)}"
  image = "${var.image}"
  name  = "${format("var.name-%s-%d", element(var.zones, count.index), count.index + 1)}"
  type  = "${var.type}"
  size  = "${var.size}"
  zone  = "${element(var.zones, count.index)}"
}
