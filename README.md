# tf-gce-disks
This module is for creating Cloud Persistent Disks.

## Usage
Declare a module in your Terraform file, for example:
```
module "tf-gce-disks" {
  source = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-gce-disks.git?ref=0.7.11-1"

}
```

Note the `ref=0.7.11-1` at the end of the `source` URL, this controls the version of this module that you import.

### Required Variables
* `name` - The base name of the disk. The final name will be a concatenation of `name-region-index`
* `size` - The size in GB of the disk, minimum `10`

### Optional Variables
* `type` - `pd-standard`|`pd-ssd`|`local-ssd` (*default*: `pd-standard`)
* `zones` - List of [Google zones ](https://cloud.google.com/compute/docs/regions-zones/regions-zones#available) (*default*: `["europe-west1-b","europe-west1-c","europe-west1-d"]`)
* `num_disks` - Number of disks to create per zone (*default*:1)
* `image` - Image from which to initialise the disk
* `snapshot` - Snapshot from which to initailie the disk

### Outputs
* `disks` - List of disk names created in the form of `name-region-index`
