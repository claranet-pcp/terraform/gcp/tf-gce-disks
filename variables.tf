variable "name" {}
variable "size" {}

variable "image" {
  default = ""
}

variable "snapshot" {
  default = ""
}

variable "zones" {
  type    = "list"
  default = ["europe-west1-b","europe-west1-c","europe-west1-d"]
}

variable "num_disks" {
  default = 1
}

variable "type" {
  default = "pd-standard"
}
